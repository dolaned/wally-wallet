# https://github.com/BitcoinUnlimited/gitlab-ci-android
image: andrewstone/nexaadk:latest

# These "stages" are actually separately initiated VMs, so better used for different configurations, etc
stages:
  - docs
  - lint
  - test
  - coverage
  - assemble
  - build # This job should be last before deploy because it runs for one hour. This job can be decoupled (see taskTree).
  - deploy_internally
  - deploy_beta

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_REF_PROTECTED == "true"
      when: always

before_script:
  - GIT_COMMIT_HASH=$(git rev-parse --short HEAD)
  - export GIT_COMMIT_HASH
  - 'echo "Current Git commit hash: $GIT_COMMIT_HASH"'
  - ./gradlew generateVersionFile

variables:
  VERSION_CODE_RAW: ${CI_PIPELINE_IID}+${VERSION_BUMPER}
  VERSION_CODE_RENDER: $(($VERSION_CODE_RAW))

tasks:
  stage: docs
  tags:
    - m1-baremetal
  script:
    - ./gradlew tasks

taskTree:
  stage: docs
  variables:
    ANDROID_HOME: "/opt/homebrew/share/android-commandlinetools"
  tags:
    - m1-baremetal
  script:
    - ./gradlew build taskTree # Print taskTree for ./gradlew build

kDoctor:
  stage: docs
  tags:
    - m1-baremetal
  script:
    - brew install kdoctor
    - kdoctor

lintVitalRelease:
  stage: lint
  tags:
    - m1-baremetal
  variables:
    ANDROID_HOME: "/opt/homebrew/share/android-commandlinetools"
  script:
    - ./gradlew :src:lintVitalRelease

lint:
  stage: lint
  tags:
    - m1-baremetal
  variables:
    ANDROID_HOME: "/opt/homebrew/share/android-commandlinetools"
  script:
    - ./gradlew lint -x :src:lintDebug # Skips android linter with 356 errors, 500 warnings.

appJar:
  stage: assemble
  script:
    - ./gradlew appJar
  artifacts:
    paths:
      - src/build/libs/wpw.jar

build:
  stage: build
  tags:
    - m1-baremetal
  rules: # this job is very slow (90 min ++). Therefore we restrict this job to run only on MR approval and always on the main branch or triggered manually.
    # Run the job on the main branch
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: on_success # Will only run if all previous jobs in the pipeline complete successfully.
    # Run the job if the merge request is approved
    - if: '$CI_MERGE_REQUEST_APPROVED == "true"'
      when: on_success # Will only run if all previous jobs in the pipeline complete successfully.
    # Allow manual execution for merge request pipelines
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: manual
    # Allow manual execution as a fallback if the above rules are not met
    - when: manual
  variables:
    ANDROID_HOME: "/opt/homebrew/share/android-commandlinetools"
  script:
    # Exclude the task :src:testDebugUnitTest and :src:testReleaseUnitTest because (I think) they do not support multiplatform projects
    - ./gradlew build -x :src:testDebugUnitTest -x :src:testReleaseUnitTest
  artifacts:
    when: always
    paths:
      - src/build/reports/tests/testDebugUnitTest/index.html
      - src/build/reports/tests/testDebugUnitTest/**/**.html
      - src/build/reports/lint-results-debug.html

compileIos:
  stage: assemble
  tags:
    - m1-baremetal
  script:
    - ./gradlew :src:compileKotlinIosArm64
    - ./gradlew :src:compileKotlinIosSimulatorArm64
    - ./gradlew :src:compileKotlinIosX64
    - ./gradlew :src:iosArm64MetadataElements
    - ./gradlew :src:iosSimulatorArm64MetadataElements
    - ./gradlew :src:iosX64MetadataElements

jvmJar:
  stage: assemble
  tags:
    - m1-baremetal
  script:
    - ./gradlew :src:jvmJar

jvmJar-intelmac:
  stage: assemble
  tags:
    - intelmac-baremetal
  script:
    - ./gradlew :src:jvmJar

iosArm64Test:
  stage: test
  tags:
    - m1-baremetal
  resource_group: iphone_mac_emulators
  script:
    - ./gradlew :src:cleanIosSimulatorArm64Test :src:iosSimulatorArm64Test
  artifacts:
    when: always
    paths:
      - src/build/reports/tests/iosSimulatorArm64Test/index.html
      - src/build/reports/tests/iosSimulatorArm64Test/**/**.html

iosX64Test:
  stage: test
  tags:
    - m1-baremetal
  script:
    - ./gradlew :src:iosX64Test

iosEmulatorUiTest:
  stage: test
  resource_group: iphone_mac_emulator
  tags:
    - m1-baremetal
  script:
    - cd iosApp
    - gem install bundler
    - gem install fastlane
    - bundle install
    - bundle update
    - bundle exec fastlane ios test_emulator
  artifacts:
    when: always
    reports:
      junit: iosApp/fastlane/report.xml
    paths:
      - iosApp/fastlane/report.xml
      - iosApp/fastlane/test_output/report.html
      - iosApp/fastlane/test_output/report.junit

iosDeviceUiTest:
  stage: test
  resource_group: iphone_mac_emulator
  tags:
    - m1-baremetal
  script:
    - cd iosApp
    - gem install xcpretty
    - ./rundevicetests.sh $DEVICE_UDID
  artifacts:
    reports:
      junit: iosApp/iphone-ui-test-report.xml
    expire_in: 4 week

jvmTest-ubuntu-docker:
  stage: test
  script:
    # https://github.com/BitcoinUnlimited/gitlab-ci-android/pull/3
    # TODO:
    #   1. Merge this^ MR and deploy andrewstone/nexaadk:latest with boost installed
    #   2. Delete the apt-get update and install script from this job.
    - apt-get update
    - apt-get install -y build-essential libboost-all-dev
    - ./gradlew :src:cleanJvmTest :src:jvmTest
  artifacts:
    reports:
      junit: src/build/test-results/jvmTest/TEST-*.xml

jvmTest-m1: #
  stage: test
  tags:
    - m1-baremetal
  script:
    - ./gradlew :src:cleanJvmTest :src:jvmTest
  artifacts:
    reports:
      junit: src/build/test-results/jvmTest/TEST-*.xml

jvmTestCoverageXml:
  stage: test
  tags:
    - intelmac-baremetal
  script:
    - ./gradlew :src:koverXmlReportJvm # koverXmlReportJvm runs jvmTest as a child job
  artifacts:
    reports:
      coverage_report:
        coverage_format: jacoco
        path: src/build/reports/kover/reportJvm.xml
      junit: src/build/test-results/jvmTest/TEST-*.xml
      # cobertura: src/build/reports/kover/reportJvm.xml
      # Maybe this? https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization/jacoco.html
    paths:
      - src/build/reports/kover/reportJvm.xml
      - src/build/reports/tests/jvmTest/**/**

jvmTestCoverageLog: #
  stage: test
  tags:
    - intelmac-baremetal
  script:
    - ./gradlew :src:koverLogJvm # koverLogJvm runs jvmTest as a child job
  coverage: '/application line coverage: (\d+\.\d+)%/' # Regex to extract coverage percentage

jvmTest-intelmac:
  stage: test
  tags:
    - intelmac-baremetal
  script:
    - ./gradlew :src:cleanJvmTest :src:jvmTest
  artifacts:
    reports:
      junit: src/build/test-results/jvmTest/TEST-*.xml
    paths:
      - src/build/reports/tests/jvmTest/**/**

# Runs Compose UI tests for Android on a Pixel 5 emulator
androidPixel5Test:
  stage: test
  tags:
    - m1-baremetal # default docker image is not working
  resource_group: pixel5_mac_emulators
  script:
    - ./gradlew pixel5Check
  artifacts:
    when: always
    paths:
      - src/build/reports/androidTests/managedDevice/debug/pixel5/index.html
      - src/build/reports/androidTests/managedDevice/debug/pixel5/**/**

# Randomly clicks around for ten minutes
# Requires an Android device to be connected to the m1-baremetal runner
androidMonkeyTest:
  stage: test
  tags:
    - m1-baremetal # Requires an Android device to be connected to the target runner
  resource_group: android_device
  when: manual # Requires the connected device to be safe for absolute random clicks anywhere in the OS and any app.
  needs:
    - androidPixel5Test
  before_script:
    # Install adb
    - brew install android-platform-tools

    # Verify adb installation
    - adb version
    - adb devices
  script:
    - adb shell monkey -p info.bitcoinunlimited.www.wally --throttle 500 -v 1000

# Deploys an apk to:
# https://gitlab.com/wallywallet/wallet/-/jobs/artifacts/main/wally.apk?job=deployAndroidApk
deployAndroidApk:
  needs:
    - androidPixel5Test
  script:
    - ./gradlew assembleRelease
    - cp src/build/outputs/apk/release/src-release.apk wally.apk
  artifacts:
    paths:
      - wally.apk
  rules:
    - if: '$CI_COMMIT_TAG =~ /^release[0-9]+\.[0-9]+\.[0-9]+$/'

testflight:
  rules:
        - if: $CI_COMMIT_REF_PROTECTED == "true"
  stage: deploy_internally
  resource_group: testflight-deployment
  tags:
    - m1-baremetal
  script:
    - mkdir iosApp/fastlane/keys
    # setting up ".env" file
    - echo "APP_STORE_CONNECT_API_KEY_PATH=$APP_STORE_CONNECT_API_KEY_PATH" >> iosApp/.env
    - echo "APPLE_ID=$APPLE_ID" >> iosApp/.env
    - echo "ITC_TEAM_ID=$ITC_TEAM_ID" >> iosApp/.env
    - echo "TEAM_ID=$TEAM_ID" >> iosApp/.env
    # setting up fastlane keys: json
    # see this for the correct quoting
    # https://docs.gitlab.com/ee/ci/yaml/script.html#use-special-characters-with-script
    - echo $KEYSJSON | base64 --decode > iosApp/fastlane/keys/$key_id.json
    # setting up fastlane keys: p8
    - echo $KEYSP8 | base64 --decode > iosApp/fastlane/keys/$key_id.p8
    # Run fastlane
    - cd iosApp/
    - gem install bundler
    - gem install fastlane
    - bundle install
    - bundle update
    - bundle exec fastlane ios testflight_internal --verbose

#testflight_public:
#  environment:
#    name: "beta deploy"
#  allow_failure: false
#  only:
#    - main
#  when: manual
#  stage: deploy_beta
#  tags:
#    - m1-baremetal
#  script:
#    - mkdir iosApp/fastlane/keys
#    # setting up ".env" file
#    - echo "APP_STORE_CONNECT_API_KEY_PATH=$APP_STORE_CONNECT_API_KEY_PATH" >> iosApp/.env
#    - echo "APPLE_ID=$APPLE_ID" >> iosApp/.env
#    - echo "ITC_TEAM_ID=$ITC_TEAM_ID" >> iosApp/.env
#    - echo "TEAM_ID=$TEAM_ID" >> iosApp/.env
#    # setting up fastlane keys: json
#    # see this for the correct quoting
#    # https://docs.gitlab.com/ee/ci/yaml/script.html#use-special-characters-with-script
#    - echo $KEYSJSON | base64 --decode > iosApp/fastlane/keys/$key_id.json
#    # setting up fastlane keys: p8
#    - echo $KEYSP8 | base64 --decode > iosApp/fastlane/keys/$key_id.p8
#    # Run fastlane
#    - cd iosApp/
#    - gem install bundler
#    - gem install fastlane
#    - bundle install
#    - bundle update
#    - bundle exec fastlane ios beta_public --verbose
