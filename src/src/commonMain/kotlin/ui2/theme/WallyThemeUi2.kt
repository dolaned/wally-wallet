package info.bitcoinunlimited.www.wally.ui2.themeUi2

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.*
import info.bitcoinunlimited.www.wally.ui2.theme.BaseBkg
import info.bitcoinunlimited.www.wally.ui2.theme.WallyBorder
import info.bitcoinunlimited.www.wally.ui2.theme.WallyBoringButtonShadow

val colorPrimaryUi2 = Color(0xFFD0A6FF)
val colorPrimaryDarkUi2 = Color(0xFF5B276B)
val wallyPurple = Color(0xFF735092)
val wallyPurple2 = Color(0xFF5F3C7E)
val wallyPurpleLight = Color(0xFFDDDAF3)
val wallyPurpleExtraLight = Color(0xFFF9F8FF)
val wallyBeige = Color(0xFFFFFCF0D9)
val wallyBeige2 = Color(0xFFF9F3E6)

val samsungKeyBoardGray= Color(0xFFf4f6f8)

val wallyLightColors = lightColorScheme(
  background = wallyBeige, // Change to Color.White when fixing colors.
)

@Deprecated("Use theme or set font size directly in Text Composable")
val defaultFontSize = 16.sp // Fallback size

var WallyAssetRowColors = arrayOf(Color(0x4Ff5f8ff), Color(0x4Fd0d0ef))

val WallyPageBase = Modifier.fillMaxSize().background(BaseBkg)

val WallyRoundedButtonOutline = BorderStroke(1.dp, WallyBorder)

val WallyBoringButtonOutline = BorderStroke(0.5.dp, WallyBoringButtonShadow)

val WallyModalOutline = BorderStroke(2.dp, WallyBorder)

@Composable
expect fun WallyThemeUi2(
    darkTheme: Boolean = false,
    dynamicColor: Boolean = false,
    typography: Typography = Typography(
    bodyLarge = MaterialTheme.typography.bodyLarge,
    headlineSmall = TextStyle(
      color = wallyPurple,
      fontSize = 22.sp,
      fontWeight = FontWeight.Bold
    ),
    headlineMedium = TextStyle(
      color = wallyPurple,
      fontSize = 28.sp
    ),
    headlineLarge = TextStyle(
      color = wallyPurple,
      fontSize = 32.sp
    ),
    titleLarge = TextStyle(
      color = wallyPurple,
      fontWeight = FontWeight.Bold,
      fontSize = 22.sp,
      lineHeight = 28.sp,
      letterSpacing = 0.sp
    ),
    labelSmall = MaterialTheme.typography.labelSmall.copy(
      color = wallyPurple
    )
  ),
    lightColors: ColorScheme = wallyLightColors,
    content: @Composable () -> Unit
)
